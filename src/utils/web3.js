import Web3 from 'web3';
import WalletConnectProvider from '@walletconnect/web3-provider';

// Init Metamask
async function connectMetaMask() {
    if (typeof window.ethereum !== 'undefined') {
        try {
            await window.ethereum.request({ method: 'eth_requestAccounts' });
            await switchToCustomNetwork();
            const web3 = new Web3(window.ethereum);
            const accounts = await web3.eth.getAccounts();
            return accounts[0]
        } catch (error) {
            console.error("MetaMask Error:", error);
        }
    } else {
        console.log('MetaMask is not installed!');
    }
}

// Init WalletConnect
async function connectWalletConnect() {
    const walletConnectProvider = new WalletConnectProvider({
        infuraId: "9b1ad215a235497eb8bec0a2e76f84a4",
    });

    try {
        await walletConnectProvider.enable();
        const web3 = new Web3(walletConnectProvider);
    } catch (error) {
        console.error("WalletConnect Error:", error);
    }
}

const switchToCustomNetwork = async () => {
    try {
        await window.ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: '0x7A69' }],
        });
    } catch (switchError) {
        if (switchError.code === 4902) {
            try {
                await window.ethereum.request({
                    method: 'wallet_addEthereumChain',
                    params: [{
                        chainId: '0x7A69',
                        chainName: 'Hardhat testnet',
                        nativeCurrency: {
                            name: 'Go',
                            symbol: 'GO',
                            decimals: 18,
                        },
                        rpcUrls: ['http://127.0.0.1:8545/'],
                    }],
                });
            } catch (addError) {
                console.error('Erreur lors de l\'ajout du réseau personnalisé', addError);
            }
        }
    }
};

export { connectMetaMask, connectWalletConnect, switchToCustomNetwork };
