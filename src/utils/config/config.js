import {
    hardhat,
} from "wagmi/chains";
import {defaultWagmiConfig} from "@web3modal/wagmi/react/config";
import {createWeb3Modal} from "@web3modal/wagmi/react";

const chains = [
    hardhat,
];

const projectId = process.env.NEXT_PUBLIC_PROJECT_ID || "";

const metadata = {
    name: "",
    description: "",
    url: "",
    icons: [""],
};

export const wagmiConfig = defaultWagmiConfig({
    chains,
    projectId,
    metadata,
});

createWeb3Modal({
    wagmiConfig: wagmiConfig,
    projectId,
})