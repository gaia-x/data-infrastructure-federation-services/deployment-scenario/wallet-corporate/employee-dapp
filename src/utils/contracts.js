import { useAccount, useContractRead, useWriteContract } from 'wagmi'
import {abi} from '/src/utils/abi/corporateSBT'

const contractAddress = process.env.NEXT_PUBLIC_CONTRACT_ADDRESS || "";

export function useGetEmployeeTokens() {
    const { address } = useAccount();
    return useContractRead({
        address: contractAddress,
        abi: abi,
        functionName: 'getMyCompanyCredentials',
        account: address,
    });
}