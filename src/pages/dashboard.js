import React, { useState, useEffect } from 'react';
import {useAccount, useConnect} from "wagmi";
import Header from "/src/components/ui/header";
import {useRouter} from "next/router";
import {useGetEmployeeTokens, useGetAddress} from "/src/utils/contracts"
import SearchBar from "/src/components/ui/search";
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '/src/styles/dashboard.module.css';
import Pagination from '/src/components/ui/pagination';

const Dashboard = () => {
    const { isConnected, address} = useAccount()
    const router = useRouter();
    const employeeCredentials = useGetEmployeeTokens();
    const [searchValue,setSearchValue]=useState("")
    const [filterData,setFilterData]=useState([])
    const [searchData, setSearchData] = useState(false)
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(0);
    const pageSize = 8;

    useEffect(() => {
        if (!isConnected) {
            router.push('/');
        }
    }, [isConnected, router]);

    useEffect(() => {
        const isLoadingOrPending = employeeCredentials.isPending || employeeCredentials.isLoading;
        setLoading(isLoadingOrPending);
    }, [employeeCredentials.isPending, employeeCredentials.isLoading]);

    async function handleValueChange(data) {
        setFilterData({
            "search": data
        });
        setSearchData(true);
        //TODO a réactiver lors de l'implementation du search
        //setLoading(true);
    }

    function handleChange(pageData) {
        setCurrentPage(pageData);
    }

    const currentCredentials = employeeCredentials.data?.slice(currentPage * pageSize, (currentPage + 1) * pageSize);

    return (
        <>
            <Header/>
            <div className={`row ${styles.docSearch} align-items-center py-2 py-lg-0 gap-lg-0 gap-3`}>
                <div className="col-lg-8 d-flex justify-content-start"></div>
                <div className='col-lg-4'>
                    <SearchBar  handleValueChange={handleValueChange}/>
                </div>
            </div>
            {loading ? (
                <div className={styles.spinner}></div>
            ) : (
                employeeCredentials.data?.length > 0 ? (
                    <Pagination
                        credentials={currentCredentials}
                        searchData={searchData}
                        handleChange={setCurrentPage}
                        page={currentPage}
                        count={employeeCredentials.data.length}
                        pageSize={pageSize}
                    />
                ) : (
                    <div className={styles.wrapper}>
                        <div className={`card shadow-sm`}>
                            <div className="card-body">
                                No documents in your possession.
                            </div>
                        </div>
                    </div>
                )
            )}
        </>
    );
};

export default Dashboard;
