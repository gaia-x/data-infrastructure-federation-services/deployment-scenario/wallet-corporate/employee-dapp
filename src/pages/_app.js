import "/src/styles/globals.css";
import {createWeb3Modal} from "@web3modal/wagmi/react";
import {GlobalProvider} from '/src/components/context/globalContext';
import {WagmiProvider} from "wagmi";
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import {useEffect, useState} from "react";
import { wagmiConfig } from "/src/utils/config/config"

const queryClient = new QueryClient();

export default function _app({Component, pageProps, initialState}) {
    const [ready, setReady] = useState(false);

    useEffect(() => {
        setReady(true);
    }, []);

    return (
        <GlobalProvider>
            {ready ? (
                <WagmiProvider  config={wagmiConfig}>
                    <QueryClientProvider client={queryClient}>
                            <Component {...pageProps} />
                    </QueryClientProvider>
                </WagmiProvider >
            ) : null}
        </GlobalProvider>
    );
}
