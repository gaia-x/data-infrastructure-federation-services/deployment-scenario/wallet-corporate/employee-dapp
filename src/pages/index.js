import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from "next/head";
import Header from "/src/components/ui/header";
import { useAccount } from 'wagmi'

export default function Home() {
    const { isConnected} = useAccount()
    const router = useRouter();

    useEffect(() => {
        if (isConnected) {
            router.push('/dashboard');
        }
    }, [isConnected, router]);

    return (
        <>
            <Head>
                <title>Employee dApp</title>
                <meta
                    name="description"
                    content=""
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <link rel="icon" href="/GaiaxBlueLogo.svg" />
            </Head>

            <Header/>

            <main>
                <div className="login-wrapper">
                    <div className={`card shadow-sm`}>
                        <div className="card-body">
                            <h5 className="title">Employee Wallet</h5>
                            {!isConnected ? (
                                <div className="description">Welcome to Employee Wallet, the simple way to access and manage your work credential. <br/><br/>
                                    Please login with your wallet.</div>
                            ) : (
                                <p className="description">Connecting...</p>
                            )}
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
}
