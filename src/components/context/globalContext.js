import React, { createContext, useContext, useState } from 'react';

const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
    const [isNetworkSwitchHighlighted, setIsNetworkSwitchHighlighted] = useState(false);
    const [isConnectHighlighted, setIsConnectHighlighted] = useState(false);

    const closeAll = () => {
        setIsNetworkSwitchHighlighted(false);
        setIsConnectHighlighted(false);
    };

    return (
        <GlobalContext.Provider value={{ isNetworkSwitchHighlighted, setIsNetworkSwitchHighlighted, isConnectHighlighted, setIsConnectHighlighted, closeAll }}>
            {children}
        </GlobalContext.Provider>
    );
};

export const useGlobalContext = () => useContext(GlobalContext);