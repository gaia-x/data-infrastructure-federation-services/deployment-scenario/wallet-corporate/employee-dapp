import React, { useState, useEffect } from "react";
import Credential from "/src/components/ui/credential";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

const Paginations = ({ credentials,reset,handleLoad,searchData,handleChange,page,count,pageSize}) => {
    const [currentPage, setCurrentPage] = useState(page>=0 ? page+1 : 1);
    const currentCredentials = credentials ? credentials : [];
    const pageCount = credentials ? Math.ceil(count / pageSize): 1;

    const handlePageClick = (event, page) => {
        setCurrentPage(page);
        handleChange(page-1)
        window.scrollTo(0, 0);
    };

    useEffect(() => {
        if (reset || searchData) {
            setCurrentPage(1);
            handleChange(0)
            handleLoad();
        }
        if (currentPage > pageCount && pageCount > 0) {
            setCurrentPage(pageCount);
            handleChange(pageCount-1);
        } else{
            setCurrentPage(currentPage);
            handleChange(currentPage-1);
        }
    }, [currentPage])

    return (
        <>
            <div className="container" >
                <div className="row">
                    {currentCredentials.map((credential, index) => {
                        return <Credential key={index} credential={credential}/>
                    })}
                </div>
            </div>
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center",marginBottom:'15px'}}>
                <Stack sx={{mt:'20px'}}>
                    <Pagination  siblingCount={0} sx={{
                        '& .MuiPaginationItem-page.Mui-selected': {
                            color: 'white',
                            backgroundColor:'#000F8E'
                        },
                        '& .MuiPaginationItem-page': {
                            color: '#000F8E',
                        }}} variant="outlined" count={pageCount} shape="circular" page={currentPage} onChange={handlePageClick}/>
                </Stack>
            </div>
        </>
    );
};

export default Paginations;