import React from 'react';
import styles from '/src/styles/home.module.css';
import { useGlobalContext } from '/src/components/context/globalContext';

const Header = () => {
    const { isNetworkSwitchHighlighted, isConnectHighlighted, closeAll } = useGlobalContext();

    return (
        <header>
            <div
                className={styles.backdrop}
                style={{
                    opacity: isConnectHighlighted || isNetworkSwitchHighlighted ? 1 : 0,
                }}
            />
            <div className={styles.header}>
                <div>
                    <img className="logo" src="/GaiaxBlueLogo.svg" alt="Logo" />
                </div>
                <div className={styles.buttons}>
                    <div
                        onClick={closeAll}
                        className={`${styles.highlight} ${
                            isNetworkSwitchHighlighted ? styles.highlightSelected : ``
                        }`}
                    >
                        <w3m-network-button />
                    </div>
                    <div
                        onClick={closeAll}
                        className={`${styles.highlight} ${
                            isConnectHighlighted ? styles.highlightSelected : ``
                        }`}
                    >
                        <w3m-button balance={"hide"}/>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;
