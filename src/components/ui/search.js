import React, { useEffect, useState } from "react";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";

const SearchBar = ({ handleValueChange, ...props }) => {
    const [value, setValue] = useState('');

    const handleChange = event => {
        setValue(event.target.value);
        handleValueChange(event.target.value);
    };

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
        }
    };

    return (
        <Form className="d-flex position-relative w-100 px-4 px-lg-2">
            <img alt="search icon" src="/assets/search.png" className="position-absolute search-icon" />
            <InputGroup className="mb-3">
                <FormControl
                    type="search"
                    placeholder="Search credentials"
                    className="me-2 search"
                    aria-label="Search"
                    onChange={handleChange}
                    value={value}
                    id="search-services"
                    onKeyDown={handleKeyDown}
                    {...props}
                />
            </InputGroup>
        </Form>
    );
};

export default SearchBar;
