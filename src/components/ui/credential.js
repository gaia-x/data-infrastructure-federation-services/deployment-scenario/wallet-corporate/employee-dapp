import React, {useEffect, useState} from 'react';
import styles from '/src/styles/credential.module.css'

const Credential = (props) => {
    const [vcData, setVcData] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');

    useEffect(() => {
        setIsLoading(true);
        setError('');

        const fetchData = async () => {
            try {
                const response = await fetch(props.credential.url);
                if (!response.ok) throw new Error(`Credential not found at URL: ${props.credential.url}`);
                const data = await response.json();
                setVcData(data);
            } catch (error) {
                console.error("Error fetching VC:", error);
                setError(error.toString());
            } finally {
                setIsLoading(false);
            }
        };
        fetchData();
    }, [props.credential.url]);

    let vcContent;
    if (vcData) {
        const credentialSubject = vcData["credentialSubject"];
        const type = credentialSubject ? (credentialSubject["type"] || credentialSubject["@type"] || "Type is not available") : "Credential Subject is not available";
        const issuer = vcData["issuer"] || "Issuer is not available";
        const issuanceDate = vcData["issuanceDate"] || "Issuance Date is not available";

        vcContent = (
            <>
                <p className={styles.label}>Type</p>
                <p className={styles.title}>{type}</p>
                <p className={styles.label}>Issuer</p>
                <p className={styles.text}>{issuer}</p>
                <p className={styles.label}>Issuance Date</p>
                <p className={styles.text}>{issuanceDate}</p>
            </>
        );
    }

    return (
        <div className="col-lg-3 col-md-4 col-sm-6 col-12" style={{ marginTop: '1em', display: "flex", justifyContent: "center" }}>
            <div className={`card h-100 shadow-sm ${styles.cardHoverEffect}`}>
                <div className={`card-body ${styles.cardBodyFlex}`}>
                    {isLoading ? (
                        <div className={`card-text ${styles.cardContent}`}>Loading...</div>
                    ) : error ? (
                        <div className={`card-text ${styles.cardContent}`}>
                            <p>{error}</p>
                        </div>
                    ) : (
                        <div className={`card-text ${styles.cardContent}`}>
                            {vcContent}
                        </div>
                    )}
                    <a href={props.credential.uri} target="_blank" rel="noopener noreferrer" className={`btn ${styles.btnBottom}`} style={{ backgroundColor: '#000F8E', color: 'white', pointerEvents: error ? 'none' : 'auto', opacity: error ? '0.5' : '1' }}>
                        Read Credential
                    </a>
                </div>
            </div>
        </div>
    );
};

export default Credential;
